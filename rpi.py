import RPi.GPIO as GPIO
import time

class rpi:
    # RPi interface for GPIO control
    def __init__(self):
        # Setting the pin numbering scheme
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)

    #GPIO initialization
    def init_gpio(self, gpio, state):
        init_success = 0
        try: 
            if state == "out":
                GPIO.setup(gpio, GPIO.OUT)
                init_success = 1
            elif state == "in":
                GPIO.setup(gpio, GPIO.IN)
                init_success = 1
            else:
                print("ERROR: Invalid GPIO state")
        except:
            print("ERROR: Unable to setup GPIO")
        return init_success

    #Read/write GPIO 
    def gpio_rw(self, operation, gpio, value = None):
        read = None
        if operation not in ["read", "write"]:
            return read
        if operation == "read":
            if value != None:
                return read
            GPIO.setup(gpio, GPIO.IN)
            time.sleep(0.1)
            read = GPIO.input(gpio)
        elif operation == "write":
            if value == None:
                return read
            GPIO.setup(gpio, GPIO.OUT)
            time.sleep(0.1)
            GPIO.output(gpio, value)
            read = GPIO.input(gpio)
        return read


    def configure_rpi(self):
        input_pins = input('Enter list of input GPIO\'s (space delimited):\n>> ')
        input_pins = [int(pin) for pin in input_pins.split()]
        output_pins = input('Enter list of output GPIO\'s (space delimited):\n>> ')
        output_pins = [int(pin) for pin in output_pins.split()]

        self.init_gpio(input_pins,'in')
        self.init_gpio(output_pins,'out')

    def gpio_terminate(self):
        GPIO.cleanup()
        
def main():
    board = rpi()
    board.configure_rpi()

    running = True
    try:
        while running:
            try:
                handles = int(input('1 - Configure pins\n2 - Read pin\n3 - Write to pin\n0 - Exit\n>> '))
            except TypeError:
                print('Must input an integer!')
                running = False
            if handles == 1:
                board.configure_rpi()
            elif handles == 2:
                try:
                    gpio = int(input('Enter gpio # >> '))
                except TypeError:
                    print('Must input an integer!')
                    running = False
                print(board.gpio_rw('read',gpio))
            elif handles == 3:
                try:
                    gpio = int(input('Enter gpio # >> '))
                except TypeError:
                    print('Must input an integer!')
                    running = False
                try:
                    value = int(input('Enter pin value (1/0) >> '))
                except TypeError:
                    print('Must input an integer!')
                    running = False
                print('Writing value: %d to gpio %d'%(value,gpio))
                print('Value: %d read from gpio %d'%(board.gpio_rw('write',gpio,value),gpio))
            elif handles == 0:
                running = False
    except KeyboardInterrupt:
        print('Keyboard Interrupt!')
    
    board.gpio_terminate()

if __name__ == '__main__':
    main()