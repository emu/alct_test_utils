# ALCT_Test_Utils

A new software repository for the ALCT teststand utilities. The software has been ported from delphi to python. This allows for a small upgrading of hardware used from an outdated Windows XP machine to a simple RPi with a hat for converting single ended to differential signals. 

## Scripts

Scripts are written for the following functions in ALCT testing (all done through JTAG connection):

 ### - Programming ALCT Mezzanine Firmware 
    Programs FPGA on ALCT
 ### - Programming ALCT Slow Control Firmware
    Programs slow control firmware
 ### - Single-Cable test
    Uses a special “loopback” firmware to provide a very simple validation of connectivity on the board
 ### - Delay Chips Pattern Check
    The delays are set with a data word which can be read back through the programming chain. This test does not actually check the delays, rather just that you able to write a pattern and read back the same pattern with no bit errors.
 ### - Reference Voltage Test
    Check that reference voltage is stable and correct value.
 ### - LEMO Connectors Test
    Connect LEMO cables to scope. Verifies each channel of ALCT test-strip for test pulse.
 ### - Thresholds-Linearity Test
    Verification of ability to set a DAC output correctly. The line should be totally smooth.
 ### - Specialty Test Board Delays Check
    Verify first the average delay of any board (delay ASICs vary greatly batch to batch). Checks channel to channel variation not too large.
 ### - Standby Test
    AFEB has standby mode, which powers it off. Controlled by register in slow control of FPGA.
 ### - Amplitude Test
    Connect LEMO cable to scope, 6 LEMO outputs with pulses produced (connected to AFEBs directly).
 ### - Self Test
    Simple yet important check of voltages, currents, temperatures, standby register (read and write operation), test pulse power down register, test pulse power up register, test pulse wire group mask, thesholds quick test for linearity. 


Additionally a single script, alcttest.py will run all tests available.


