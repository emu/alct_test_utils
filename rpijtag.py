import io
from gpiozero import *
from gpiozero.pins.mock import MockFactory
import time

#-----------------------------------------------------------------------------
# Default pin mapping - using BCM numbering (GPIO numbers)
#-----------------------------------------------------------------------------
# JTAG TAP Controller
TMS = 4
TCK = 17
TDO = 27
TDI = 22
# JTAG Chain mux
S0   = 10
S1   = 9
DCLK = 11

# ----------------------------------------------------------------------------
# JTAG States
# ----------------------------------------------------------------------------
RESET           = 0x0
IDLE            = 0x1
SELECT_DR_SCAN  = 0x2
CAPTURE_DR      = 0x3
SHIFT_DR        = 0x4
EXIT1_DR        = 0x5
PAUSE_DR        = 0x6
EXIT2_DR        = 0x7
UPDATE_DR       = 0x8
SELECT_IR_SCAN  = 0x9
CAPTURE_IR      = 0xA
SHIFT_IR        = 0xB
EXIT1_IR        = 0xC
PAUSE_IR        = 0xD
EXIT2_IR        = 0xE
UPDATE_IR       = 0xF
UNKNOWN          = None

#-----------------------------------------------------------------------------
# JTAG Chains
#-----------------------------------------------------------------------------

# JTAG Register codes
IR_REG = 0
DR_REG = 1

def is_rpi():
    try:
        with io.open('/sys/firmware/devicetree/base/model', 'r') as m:
            if 'raspberry pi' in m.read().lower():
                return True
    except FileNotFoundError: 
        pass
    return False

if not is_rpi():
    Device.pin_factory = MockFactory()

class RPi:
    def __init__(self,tms,tck,tdo,tdi,s0,s1,dclk) -> None:
        self.gpio_map = {
            'tms'   : tms,
            'tck'   : tck,
            'tdo'   : tdo,
            'tdi'   : tdi,
            's0'    : s0,
            's1'    : s1,
            'dclk'  : dclk
        }
        self.init_jtag()
        self.init_chain()

    def init_jtag(self):
        self.jtag = CompositeDevice(
            tms = DigitalOutputDevice(self.gpio_map['tms'], initial_value=1),
            tck = DigitalOutputDevice(self.gpio_map['tck'], initial_value=0),
            tdo = DigitalInputDevice(self.gpio_map['tdo'],  pull_up=0), # pull down
            tdi = DigitalOutputDevice(self.gpio_map['tdi'], initial_value=0)
        )
    def init_chain(self):
        self.chain = CompositeOutputDevice(
                # 4 data bits for setting jtag chain (little endian)
                s0 = DigitalOutputDevice(self.gpio_map['s0'],     initial_value=0),
                s1 = DigitalOutputDevice(self.gpio_map['s1'],     initial_value=0),
                dclk = DigitalOutputDevice(self.gpio_map['dclk'], initial_value=0)
            )

class RPiJTAG(RPi):
    def __init__(
        self,
        tms = TMS,
        tck = TCK,
        tdo = TDO,
        tdi = TDI,
        s0  = S0,
        s1  = S1,
        dclk = DCLK,
    ):
        '''
        Initializes RPi GPIO's for JTAG controller. Default values are assigned using constants, but can be altered with kwargs.\n
        Optional Parameters:
        `tms`:  RPi GPIO number for TMS signal
        `tck`:  RPi GPIO number for TCK signal
        `tdo`:  RPi GPIO number for TDO signal
        `tdi`:  RPi GPIO number for TDI signal
        `tdi`:  RPi GPIO number for TDI signal
        `s0`:   RPi GPIO number for JTAG_SEL0 signal
        `s1`:   RPi GPIO number for JTAG_SEL1 signal
        `dclk`: RPi GPIO number for DCLK signal
        '''

        super().__init__(tms,tck,tdo,tdi,s0,s1,dclk)

        self._current_state = UNKNOWN
        self._state_names = {
            RESET           : 'TEST_LOGIC_RESET',
            IDLE            : 'RUN_TEST_IDLE',
            SELECT_DR_SCAN  : 'SELECT_DR_SCAN',
            CAPTURE_DR      : 'CAPTURE_DR',
            SHIFT_DR        : 'SHIFT_DR',
            EXIT1_DR        : 'EXIT1_DR',
            PAUSE_DR        : 'PAUSE_DR',
            EXIT2_DR        : 'EXIT2_DR',
            UPDATE_DR       : 'UPDATE_DR',
            SELECT_IR_SCAN  : 'SELECT_IR_SCAN',
            CAPTURE_IR      : 'CAPTURE_IR',
            SHIFT_IR        : 'SHIFT_IR',
            EXIT1_IR        : 'EXIT1_IR',
            PAUSE_IR        : 'PAUSE_IR',
            EXIT2_IR        : 'EXIT2_IR',
            UPDATE_IR       : 'UPDATE_IR',
            UNKNOWN         : 'UNKNOWN'
        }
        self._handlers = {
            RESET           : self.test_logic_reset,
            IDLE            : self.run_test_idle,
            SELECT_DR_SCAN  : self.select_dr_scan,
            CAPTURE_DR      : self.capture_dr,
            SHIFT_DR        : self.shift_dr,
            EXIT1_DR        : self.exit1_dr,
            PAUSE_DR        : self.pause_dr,
            EXIT2_DR        : self.exit2_dr,
            UPDATE_DR       : self.update_dr,
            SELECT_IR_SCAN  : self.select_ir_scan,
            CAPTURE_IR      : self.capture_ir,
            SHIFT_IR        : self.shift_ir,
            EXIT1_IR        : self.exit1_ir,
            PAUSE_IR        : self.pause_ir,
            EXIT2_IR        : self.exit2_ir,
            UPDATE_IR       : self.update_ir,
            UNKNOWN         : self.unknown
        }

        # Initialize jtag chain and state machine
        self.pulse_dclk()
        self.TestLogicReset()
    
    #-----------------------------------------------------------------------------
    # RPi utils
    #-----------------------------------------------------------------------------

    def close(self):
        self.jtag.close()
        self.chain.close()

    def pulse_tck(self,on:bool=None):
        if on is None:
            # check if tck is already high
            if self.jtag.tck.value:
                # pull low
                self.jtag.tck.value = 0
            # pulse tck
            self.jtag.tck.value = 1
            self.jtag.tck.value = 0
        elif on:
            # toggle tck high
            self.jtag.tck.value = 1
        else:
            # toggle tck low
            self.jtag.tck.value = 0

    def pulse_dclk(self):
        # check if dclk is already high
        if self.chain.dclk.value:
            # pull low
            self.chain.dclk.value = 0
        # pulse dclk
        self.chain.dclk.value = 1
        self.chain.dclk.value = 0

    def set_jtag_chain(self,chain:int):
        '''
        Sets 2-bit D-FF that controls JTAG chain mux on ALCT board\n
        Parameters:
        `chain`: 2-bit wide int value to set.\n
        JTAG Chains:
        `SLOWCTL_CONTROL` = 0x0 (Slow Control Control)\\
        `SLOWCTL_PROGRAM` = 0x1 (Slow Control Programming)\\
        `VIRTEX_CONTROL`  = 0x2 (Virtex Control)\\
        `VIRTEX_PROGRAM`  = 0x3 (Virtex Programming)
        '''
        # Parse chain into binary array and set D-flip-flop value
        self.chain.value = [0] + [(chain>>i)%2 for i in range(2)]
        self.pulse_dclk()
        self.chain.value = 0,0,0

    #-----------------------------------------------------------------------------
    # JTAG
    # ---------------
    # RPi bit-banging
    #-----------------------------------------------------------------------------
    
    def jtag_io(self,tms_value:int,tdi_value:int=None):
        '''Set TMS and TDI values then pulse TCK, return TDO value\n
        Parameters:
        `tms_value`: TMS pin value (1/0)
        `tdi_value`: TDI pin value (1/0)
        Returns:
        `tdo_value`: TDO pin read value
        '''
        # Set TMS,TDI pins then pulse clock
        self.jtag.tms.value = tms_value
        self.jtag.tdi.value = tdi_value
        self.pulse_tck(1)
        time.sleep(0.001)
        # read data before setting tclk low
        tdo_value = self.jtag.tdo.value
        self.pulse_tck(0)
        # Update internal state
        self.set_tap_state(tms_value)
        return tdo_value
    
    def ShiftData(self,Data:int,DataSize:int,sendtms:bool=False):
        '''
        Shift Data into JTAG Register, bit-by-bit\n
        Parameters:
        `Data`: Data to be written
        `DataSize`: Number of bits to send
        `sendtms`: True: Send tms=1 on last bit or False: override exit shift register
        '''
        # Create list of binary values in little endian order
        # If size(Data)>DataSize the most significant bits will be clipped
        binary_data = [(Data>>i) & 0x1 for i in range(DataSize)]
        result = 0
        # Send TDI one bit at a time - Little endian (LSB first)
        for i,tdi in enumerate(binary_data):
            # TMS should get HIGH on the last bit of the last byte of an instruction
            # Otherwise it should be LOW
            if (sendtms) and (i==(DataSize - 1)):
                tms = 1
            else:
                tms = 0

            # write data and get TDO value
            tdo = self.jtag_io(tms,tdi)

            # Shift TDO bit to get byte
            result |= tdo << i

        return result
    
    def IOExchange(self,Send,DataSize,RegType):
        '''
        Write data to Instruction or Data registers and return TDO data, one byte at a time.\n
        Parameters:
        `Send`: Data to be sent.
        `DataSize`: Number of bits to send. If greater than 1 byte, data will be split into 1byte chunks
        `RegType`: Type of register: Instruction or Data.
        '''
        # Write 1 byte at a time
        ChunkSize = 8        
        # Number of words needed to shift entire data = ceiling(DataSize/ChunkSize)
        nChunks = -(DataSize // -ChunkSize)

        # initiate data shift
        if (RegType == IR_REG):
            self.StartIRShift()
        elif (RegType == DR_REG):
            self.StartDRShift()        

        Recv=0 # Received Data (reconstructed Full-packet)
        for i in range(nChunks):
            size = min(DataSize-ChunkSize*i,ChunkSize)
            mask = 2**size - 1 # 0xFF if sending byte,
            # Set tms high on last bit of last chunk
            sendtms = i==nChunks-1
            data = (Send >> ChunkSize*i) & mask
            tdo_byte = self.ShiftData(data,size,sendtms)
            Recv |= tdo_byte << (ChunkSize*i)

        if RegType == IR_REG:
            self.ExitIRShift()
        elif RegType == DR_REG:
            self.ExitDRShift()

        return Recv 

    #-----------------------------------------------------------------------------
    # TAP Finite State Machine
    #-----------------------------------------------------------------------------

    def TestLogicReset(self):
        # Pulse TMS high 5 times to place TAP controller in reset mode
        for i in range(5):
            self.jtag_io(1)
        self._current_state = RESET
    
    def StartIRShift(self):
        self.jtag_io(0)
        self.jtag_io(1)
        self.jtag_io(1)
        self.jtag_io(0)
        self.jtag_io(0)
    
    def ExitIRShift(self):
        self.jtag_io(1)
        self.jtag_io(0)
        self.jtag_io(0)
        self.jtag_io(0)
    
    def StartDRShift(self):
        self.jtag_io(0)
        self.jtag_io(1)
        self.jtag_io(0)
        self.jtag_io(0)
    
    def ExitDRShift(self):
        self.jtag_io(1)
        self.jtag_io(0)
        self.jtag_io(0)
        self.jtag_io(0)

    def get_tap_state(self):
        return self._state_names[self._current_state]

    def set_tap_state(self,tms):
        self._current_state = self._handlers[self._current_state](tms)

    def test_logic_reset(self,tms):
        if tms:
            return RESET
        else:
            return IDLE
    
    def run_test_idle(self,tms):
        if tms:
            return SELECT_DR_SCAN
        else:
            return IDLE

    def select_dr_scan(self,tms):
        if tms:
            return SELECT_IR_SCAN
        else:
            return CAPTURE_DR
    
    def capture_dr(self,tms):
        if tms:
            return EXIT1_DR
        else:
            return SHIFT_DR
    
    def shift_dr(self,tms):
        if tms:
            return EXIT1_DR
        else:
            return SHIFT_DR
    
    def exit1_dr(self,tms):
        if tms:
            return UPDATE_DR
        else:
            return PAUSE_DR
    
    def pause_dr(self,tms):
        if tms:
            return EXIT2_DR
        else:
            return PAUSE_DR
    
    def exit2_dr(self,tms):
        if tms:
            return UPDATE_DR
        else:
            return SHIFT_DR
    
    def update_dr(self,tms):
        if tms:
            return SELECT_DR_SCAN
        else:
            return IDLE
    
    def select_ir_scan(self,tms):
        if tms:
            return RESET
        else:
            return CAPTURE_IR
    
    def capture_ir(self,tms):
        if tms:
            return EXIT1_IR
        else:
            return SHIFT_IR
    
    def shift_ir(self,tms):
        if tms:
            return EXIT1_IR
        else:
            return SHIFT_IR
    
    def exit1_ir(self,tms):
        if tms:
            return UPDATE_IR
        else:
            return PAUSE_IR
    
    def pause_ir(self,tms):
        if tms:
            return EXIT2_IR
        else:
            return PAUSE_IR
    
    def exit2_ir(self,tms):
        if tms:
            return UPDATE_IR
        else:
            return SHIFT_IR
    
    def update_ir(self,tms):
        if tms:
            return SELECT_IR_SCAN
        else:
            return IDLE
    
    def unknown(self,tms):
        return UNKNOWN
