from collections import namedtuple
import sys
# ------------------------------------------------------------------------------
# Mezzanine Control Addresses
# ------------------------------------------------------------------------------
Register = namedtuple('Data_Register',['ADDR','SIZE'])

# Name                  Adr     Len     Dir     Description
IDRead         = Register(0x00, 40 )  # read    Virtex ID register
HCMaskRead     = Register(0x01, 384)  # read    hot mask
HCMaskWrite    = Register(0x02, 384)  # write   hot mask
RdTrig         = Register(0x03, 5  )  # read    trigger register
WrTrig         = Register(0x04, 5  )  # write   trigger register
RdCfg          = Register(0x06, 69 )  # read    control register
WrCfg          = Register(0x07, 69 )  # write   control register
Wdly           = Register(0x0D, 120)  # write   delay lines. cs_dly bits in Par
Rdly           = Register(0x0E, 120)  # read    delay lines. cs_dly bits in Par
CollMaskRead   = Register(0x13, 224)  # read    collision pattern mask
CollMaskWrite  = Register(0x14, 224)  # write   collision pattern mask
ParamRegRead   = Register(0x15, 6  )  # read    delay line control register actually
DelayCtrlRead  = Register(0x15, 6  )  # read    Delay line control register read (LENGTH=5,5,6,9,9)
ParamRegWrite  = Register(0x16, 6  )  # read    delay line control register actually
DelayCtrlWrite = Register(0x16, 6  )  # write   Delay line control register write (LENGTH=5,5,6,9,9)
InputEnable    = Register(0x17, 0  )  # write?  commands to disable and enable input
InputDisable   = Register(0x18, 0  )  # write?  commands to disable and enable input
YRwrite        = Register(0x19, 31 )  # write   output register (for debugging with UCLA test board)
OSread         = Register(0x1A, 49 )  # read    output storage
SNread         = Register(0x1B, 1  )  # read    one bit of serial number
SNwrite0       = Register(0x1C, 0  )  # write   0 bit into serial number chip
SNwrite1       = Register(0x1D, 0  )  # write   1 bit into serial number chip
SNreset        = Register(0x1E, 0  )  # write   reset serial number chip
Bypass         = Register(0x1F, 1  )  # bypass  Bypass Register

# ------------------------------------------------------------------
# FPGA and PROM Devices
# IDCODES and Registers
# ------------------------------------------------------------------

class JTAG_Device:
    def __init__(self,idcode,id_mask,ir_size,registers,idcode2=None):
        if idcode2 is not None:
            self.ID1 = idcode,
            self.ID2 = idcode2
        else:
            self.ID = idcode
        self.ID_MASK = id_mask,
        self.IR_SIZE = ir_size,
        for reg_name,reg in registers:
            self.__setattr__(reg_name,reg)

# ------------------------------------------------------------------
# Slow Control
# ------------------------------------------------------------------
SC_FPGA_xcs40xl = JTAG_Device(
    idcode      = 0x0041C093,
    id_mask     = 0x01FFFFFFFF,
    ir_size     = 3,
    registers   = dict(
        # Name            Adr   Len     Dir     Description
        # TODO : add more registers if needed
        IDCODE  = Register(0x6, 32),   # read    FPGA ID register
        BYPASS  = Register(0x7, 1)    # bypass  Bypass register
    )
)

SC_PROM_xc18v01 = JTAG_Device(
    idcode      = 0x05024093,
    id_mask     = 0x01FFFFFFFF,
    ir_size     = 8
    registers   = dict(
        IDCODE  = Register(0xFE,32),
        Bypass  = Register(0xFF,1)
    )
)

