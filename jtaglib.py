#!/usr/bin/env python3

# ------------------------------------------------------------------------------
# Platform agnostic wrapper around the JTAG IO device of choice
# ALL IO in other modules should be done through this wrapper!!
# ------------------------------------------------------------------------------

# system imports
import sys
import os
from rpijtag import is_rpi

if os.name=='nt':
    # BACKEND = "rpi"
    BACKEND = "ctypes"
elif os.name=='posix':
    #BACKEND = "rpi"
    if is_rpi():
        BACKEND = "rpi"
    else:
        BACKEND = "python"

#DEFINES
IR_REG = 0
DR_REG = 1

if  BACKEND=="nativec": 
    import lptjtaglib

    def set_chain(chain):
        lptjtaglib.setchain(chain)

    def TestLogicReset():
        lptjtaglib.resetLPTJTAG()

    def StartIRShift():
        lptjtaglib.StartIRShift()

    def StartDRShift():
        lptjtaglib.StartDRShift()

    def ExitIRShift():
        lptjtaglib.ExitIRShift()

    def ExitDRShift():
        lptjtaglib.ExitDRShift()

    # Write JTAG Instruction Register
    def WriteIR(IR, IRSize):
        if (IRSize is None) or (IR is None):
            print("ERROR: Attempted to write invalid instruction register.")
            sys.exit()

        return IOExchange(IR, IRSize,IR_REG)

    # Write JTAG Data Register
    def WriteDR(DR, DRSize):
        if (DRSize is None) or (DR is None):
            print("ERROR: Attempted to write invalid data register.")
            sys.exit()

        return IOExchange(DR, DRSize, DR_REG)

    # Shift Data into JTAG Register, bit-by-bit
    def ShiftData(Data, DataSize, sendtms):
        # Quit if nothing is passed as data
        if (DataSize is None) or (Data is None):
            print("ERROR: Attempted to shift invalid data.")
            sys.exit()

        # Quit if data is too large
        if (DataSize < 0 or DataSize >32):
            print("ERROR: Attempted to shift too large of data (>32 bits at a time).")
            sys.exit()
        return(lptjtaglib.ShiftData(Data, DataSize, sendtms))

    #-------------------------------------------------------------------------------
    # Sends and receives JTAG data... wrapper to to Start IR/DR Shift, Shift in data
    # while reading from TDO.. exit IR/DR shift. Returns TDO data.
    #-------------------------------------------------------------------------------
    def IOExchange(Send, DataSize, RegType):
        if (DataSize is None) or (Send is None):
            print("ERROR: attempting to write nothing to IOExchange")
            sys.exit()

        Recv = lptjtaglib.IOExchange(Send, DataSize, RegType)
        return (Recv)

elif BACKEND=="ctypes":
    import ctypes

    # local imports
    path_to_lib = os.getcwd() + '/lptjtaglib/lptjtaglib'
    # Add lptjtaglib directory to path for importing dll packages
    os.environ['PATH'] = path_to_lib + os.pathsep + os.environ['PATH']
    # Import lptjtaglib using DLL
    lptjtaglib = ctypes.cdll.LoadLibrary('lptjtaglib')

    #DEFINES
    IR_REG = 0
    DR_REG = 1

    def set_chain(chain):
        lptjtaglib.setchain(chain)

    def TestLogicReset():
        lptjtaglib.TestLogicReset()

    def StartIRShift():
        lptjtaglib.StartIRShift()

    def StartDRShift():
        lptjtaglib.StartDRShift()

    def ExitIRShift():
        lptjtaglib.ExitIRShift()

    def ExitDRShift():
        lptjtaglib.ExitDRShift()

    # Write JTAG Instruction Register
    def WriteIR(IR, IRSize):
        if (IRSize is None) or (IR is None):
            print("ERROR: Attempted to write invalid instruction register.")
            sys.exit()
        print('IR size = %d'%IRSize)
        return IOExchange(IR, IRSize,IR_REG)

    # Write JTAG Data Register
    def WriteDR(DR, DRSize):
        if (DRSize is None) or (DR is None):
            print("ERROR: Attempted to write invalid data register.")
            sys.exit()
        print('DR size = %d'%DRSize)
        return IOExchange(DR, DRSize, DR_REG)

    # Shift Data into JTAG Register, bit-by-bit
    def ShiftData(Data, DataSize, sendtms):
        # Quit if nothing is passed as data
        if (DataSize is None) or (Data is None):
            print("ERROR: Attempted to shift invalid data.")
            sys.exit()

        # Quit if data is too large
        if (DataSize < 0 or DataSize >32):
            print("ERROR: Attempted to shift too large of data (>32 bits at a time).")
            sys.exit()
        result = lptjtaglib.ShiftData(Data, DataSize, sendtms)
        print('Received byte = 0x%1X'%result)
        return result
        # return(lptjtaglib.ShiftData(Data, DataSize, sendtms))

    #-------------------------------------------------------------------------------
    # Sends and receives JTAG data... wrapper to to Start IR/DR Shift, Shift in data
    # while reading from TDO.. exit IR/DR shift. Returns TDO data.
    #-------------------------------------------------------------------------------
    def IOExchange(Send, DataSize, RegType):
        #if (DataSize is None) or (Send is None):
        #    print("ERROR: attempting to write nothing to IOExchange")
        #    sys.exit()
        #return (lptjtaglib.IOExchange(Send, DataSize, RegType))

        if (DataSize is None) or (Send is None):
            print("ERROR: attempting to write nothing to IOExchange")
            sys.exit()

        # We can write 8 bits at a time
        ChunkSize   = 8

        # Number of words needed to shift entire data
        nChunks     = (abs(DataSize-1)//ChunkSize) + 1

        # instruction register or data register? 
        # initiate data shift
        if (RegType == IR_REG):
            lptjtaglib.StartIRShift()
        elif (RegType == DR_REG):
            lptjtaglib.StartDRShift()

        Recv=0 # Received Data (reconstructed Full-packet)
        tdo =0 # Test data out (byte-by-byte)

        # this whole loop can be simplified a lot.. e.g. this seems it should have the same behavior 
        # but need to test to be sure
        #for i in range(nChunks):
        #    if i==nChunks-1: 
        #        tdo = 0xFF & ShiftData(0xFF & (Send >> 8*i), DataSize - ChunkSize*i, True)
        #    else: 
        #        tdo = 0xFF & ShiftData(0xFF & (Send >> 8*i), ChunkSize,              False)
        #    Recv = Recv | tdo << (8*i)

        for i in range(nChunks):
            if (DataSize > ChunkSize*i):  # i don't think this "if" is needed
                # not the last byte sent
                if (DataSize - ChunkSize*i) > ChunkSize:
                    tdo = 0xFF & ShiftData(0xFF & (Send >> 8*i), ChunkSize,              False)
                # the last byte sent needs to have a TMS sent with it
                else:
                    tdo = 0xFF & ShiftData(0xFF & (Send >> 8*i), DataSize - ChunkSize*i, True)

            Recv = Recv | tdo << (8*i)

        if   (RegType == IR_REG):
            lptjtaglib.ExitIRShift()
        elif (RegType == DR_REG):
            lptjtaglib.ExitDRShift()

        print('Received data = 0x%X'%Recv)

        return (Recv)

elif BACKEND == "python":
    import lptjtaglib

    #DEFINES
    IR_REG = 0
    DR_REG = 1

    def set_chain(chain):
        lptjtaglib.setchainLPTJTAG(chain)

    def StartIRShift():
        lptjtaglib.StartIRShiftLPTJTAG()

    def StartDRShift():
        lptjtaglib.StartDRShiftLPTJTAG()

    def ExitIRShift():
        lptjtaglib.ExitIRShiftLPTJTAG()

    def ExitDRShift():
        lptjtaglib.ExitDRShiftLPTJTAG()

    # Write JTAG Instruction Register
    def WriteIR(IR, IRSize):
        if (IRSize is None) or (IR is None):
            print("ERROR: Attempted to write invalid instruction register.")
            sys.exit()

        return IOExchange(IR, IRSize,IR_REG)

    # Write JTAG Data Register
    def WriteDR(DR, DRSize):
        if (DRSize is None) or (DR is None):
            print("ERROR: Attempted to write invalid data register.")
            sys.exit()

        return IOExchange(DR, DRSize, DR_REG)

    # Shift Data into JTAG Register, bit-by-bit
    def ShiftData(Data, DataSize, sendtms):
        result = 0

        # Quit if nothing is passed as data
        if (DataSize is None) or (Data is None):
            print("ERROR: Attempted to shift invalid data.")
            sys.exit()

        # Quit if data is too large
        if (DataSize < 0 or DataSize >32):
            print("ERROR: Attempted to shift too large of data (>32 bits at a time).")
            sys.exit()

        for i in range(1,DataSize+1):

            # TMS should get HIGH on the last bit of the last byte of an instruction
            # Otherwise it should be LOW
            if (sendtms) and (i==DataSize):
                tms = 0x1
            else:
                tms = 0x0

            #set TDI value
            tdi = Data & (0x1)

            #write data
            tdo=lptjtaglib.jtagioLPTJTAG(tms, tdi)

            #Throw out that bit
            Data = Data >> 1

            #Read in TDO
            result = result | ((tdo & 0x01) << (i-1))

        return(result)

    #-------------------------------------------------------------------------------
    # Sends and receives JTAG data... wrapper to to Start IR/DR Shift, Shift in data
    # while reading from TDO.. exit IR/DR shift. Returns TDO data.
    #-------------------------------------------------------------------------------
    def IOExchange(Send, DataSize, RegType):
        if (DataSize is None) or (Send is None):
            print("ERROR: attempting to write nothing to IOExchange")
            sys.exit()

        # We can write 8 bits at a time
        ChunkSize   = 8

        # Number of words needed to shift entire data
        nChunks     = (abs(DataSize-1)//ChunkSize) + 1

        # instruction register or data register? 
        # initiate data shift
        if (RegType == IR_REG):
            StartIRShift()
        elif (RegType == DR_REG):
            StartDRShift()

        Recv=0 # Received Data (reconstructed Full-packet)
        tdo =0 # Test data out (byte-by-byte)

        # this whole loop can be simplified a lot.. e.g. this seems it should have the same behavior 
        # but need to test to be sure
        #for i in range(nChunks):
        #    if i==nChunks-1: 
        #        tdo = 0xFF & ShiftData(0xFF & (Send >> 8*i), DataSize - ChunkSize*i, True)
        #    else: 
        #        tdo = 0xFF & ShiftData(0xFF & (Send >> 8*i), ChunkSize,              False)
        #    Recv = Recv | tdo << (8*i)

        for i in range(nChunks):
            if (DataSize > ChunkSize*i):  # i don't think this "if" is needed
                # not the last byte sent
                if (DataSize - ChunkSize*i) > ChunkSize:
                    tdo = 0xFF & ShiftData(0xFF & (Send >> 8*i), ChunkSize,              False)
                # the last byte sent needs to have a TMS sent with it
                else:
                    tdo = 0xFF & ShiftData(0xFF & (Send >> 8*i), DataSize - ChunkSize*i, True)

            Recv = Recv | tdo << (8*i)

        if   (RegType == IR_REG):
            ExitIRShift()
        elif (RegType == DR_REG):
            ExitDRShift()

        return (Recv)

    #def open_jtag():
    #    lptjtaglib.openLPTJTAG()
    #
    #def close_jtag():
    #    lptjtaglib.closeLPTJTAG()
    #def reset_jtag():
    #    lptjtaglib.resetLPTJTAG()

    #def enable_jtag():
    #    lptjtaglib.enableLPTJTAG()

    #def idle_jtag():
    #    lptjtaglib.idleLPTJTAG()

    #def rti_jtag():
    #    lptjtaglib.rtiLPTJTAG()

    #def oneclock_jtag():
    #    lptjtaglib.oneclockLPTJTAG()

    #def TMS_High():
    #    lptjtaglib.TMSHighLPTJTAG()

    #def TMS_Low():
    #    lptjtaglib.TMSLowLPTJTAG()

    #def lights_jtag():
    #    lptjtaglib.lightsLPTJTAG()

    #def jtag_io(tms_, tdi_):
    #    tdo=lptjtaglib.jtagioLPTJTAG(tms_, tdi_)
    #    return(tdo)

elif BACKEND == "rpi":
    import rpijtag

    # Configure rpi pins
    rpi = rpijtag.RPiJTAG()

    def TestLogicReset():
        rpi.TestLogicReset()
    
    def set_chain(chain):
        rpi.set_jtag_chain(chain)

    def StartIRShift():
        rpi.StartIRShift()

    def StartDRShift():
        rpi.StartDRShift()

    def ExitIRShift():
        rpi.ExitIRShift()

    def ExitDRShift():
        rpi.ExitDRShift()

    # Write JTAG Instruction Register
    def WriteIR(IR, IRSize):
        if (IRSize is None) or (IR is None):
            print("ERROR: Attempted to write invalid instruction register.")
            sys.exit()
        return IOExchange(IR, IRSize,IR_REG)

    # Write JTAG Data Register
    def WriteDR(DR, DRSize):
        if (DRSize is None) or (DR is None):
            print("ERROR: Attempted to write invalid data register.")
            sys.exit()
        return IOExchange(DR, DRSize, DR_REG)

    def ShiftData(Data, DataSize, sendtms):
        '''Shift Data into JTAG Register, bit-by-bit\n
        Parameters:
        `Data`: Data to be written
        `DataSize`: Number of bits
        `sendtms`: True: Send tms=1 on last bit or False: override exit shift register'''
        # Quit if nothing is passed as data
        if (DataSize is None) or (Data is None):
            print("ERROR: Attempted to shift invalid data.")
            sys.exit()

        # Don't need a size limit
        # # Quit if data is too large
        # if (DataSize < 0 or DataSize >32):
        #     print("ERROR: Attempted to shift too large of data (>32 bits at a time).")
        #     sys.exit()
        
        return rpi.ShiftData(Data, DataSize, sendtms)
    
    def IOExchange(Send, DataSize, RegType):
        if (DataSize is None) or (Send is None):
            print("ERROR: attempting to write nothing to IOExchange")
            sys.exit()

        Recv = rpi.IOExchange(Send, DataSize, RegType)
        return Recv

# Common utils
def DetectDevices():
    TestLogicReset()
    # Place all devices in bypass (up to 8 devices w/ IR_SIZE=16)
    StartIRShift()
    for i in range(8):
        if i == 7:
            ShiftData(0xffff,16,True)
        else:
            ShiftData(0xffff,16,False)
    ExitIRShift()
    # Flush data registers (size = 8 devices w/ DR_SIZE=32)
    WriteDR(0,256)
    # Write 1's and get delay
    StartDRShift()
    delay = 0
    for i in range(256):
        if i==255:
            tdo = ShiftData(1,1,True)
        else:
            tdo = ShiftData(1,1,False)
        if tdo==0:
            delay += 1
        elif tdo==1:
            ShiftData(1,1,True)
            break
    ExitDRShift()
    if delay < 256:
        num_devices = delay
    else:
        num_devices = 0
    print(f"{num_devices} devices found.")
    if not num_devices:
        return

    # Get IDCODES
    TestLogicReset()
    StartDRShift()
    device_id_list = []
    for i in range(num_devices):
        if i == num_devices-1:
            idcode = ShiftData(0,32,True)
        else:
            idcode = ShiftData(0,32,False)
            
        print(f"Device {i} idcode: {hex(idcode)}")
        device_id_list.append(idcode)
    return device_id_list